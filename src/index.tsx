import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
import { CoreCtxProvider } from "./core/ctx/core-ctx";
import { createRoot } from "react-dom/client";
import "./core/config/i18n";
import "./index.css";
import { SnackbarProvider } from "notistack";
import {
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query';
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

const root = createRoot(document.getElementById("root")!);
const queryClient = new QueryClient();

root.render(
  <QueryClientProvider client={queryClient}>
    <SnackbarProvider
      maxSnack={3}
      autoHideDuration={7000}
      style={{ whiteSpace: "pre-line" }}
    >
      <BrowserRouter>
        <CoreCtxProvider>
          <App />
        </CoreCtxProvider>
      </BrowserRouter>
    </SnackbarProvider>
    <ReactQueryDevtools initialIsOpen={false} />
  </QueryClientProvider>

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
