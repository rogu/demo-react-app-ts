import { Box } from "@mui/material";
import { DOMAttributes } from "react";
import "reusable-webc/dist/footer";
import { CoreCtxConsumer } from "./core/ctx/core-ctx";
import { RouterConfig } from "./core/nav/router-config";

type CustomElement<T> = Partial<T & DOMAttributes<T> & { children: any }>;

declare global {
  namespace JSX {
    interface IntrinsicElements {
      ["course-footer"]: CustomElement<any>;
    }
  }
}

function App() {
  return (
    <CoreCtxConsumer>
      {({ state }) => (
        <Box sx={{ bgcolor: state.theme === "light" ? "#fafafa" : "#eaeaea" }}>
          <RouterConfig state={state}></RouterConfig>
        </Box>
      )}
    </CoreCtxConsumer>
  );
}

export default App;
