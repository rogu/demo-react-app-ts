import { yupResolver } from "@hookform/resolvers/yup";
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  TextField,
} from "@mui/material";
import { styled } from "@mui/styles";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import * as yup from "yup";
import Error from "../../common/ui/error";
import { useCoreCtx } from "../../core/ctx/core-ctx";

const Field = styled("div")({
  color: "darkslategray",
  display: "block",
  marginBottom: 30,
});

const schema = yup.object().shape({
  username: yup.string().required().email(),
  password: yup
    .string()
    .required()
    .matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$/),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref("password")], "passwords aren't equals"),
  hobbies: yup
    .array()
    .test("test1", "", (val: any) => {
      return val?.length
        ? true
        : new yup.ValidationError("Please check at least one checkbox");
    })
    .required()
    .nullable(),
  birthDate: yup
    .string()
    .test("testDate", "past date required", (val: string | undefined = "") => {
      return Date.parse(val) < Date.now();
    })
    .required(),
});
const hobbies = ["sport", "music", "yt", "ski", "beer"];

export function Register() {
  const {
    handleSubmit,
    formState: { errors },
    register,
    watch,
  } = useForm({ resolver: yupResolver(schema) });

  const [state, setState] = useState({});

  const { t } = useTranslation();

  const {
    registerUser,
    state: { loggedIn },
  } = useCoreCtx();

  useEffect(() => {
    watch((value) => setState(value));
  }, [watch]);

  return (
    <Box
      sx={{
        display: "flex",
        gap: 2,
        flexDirection: { sm: "row", xs: "column-reverse" },
      }}
    >
      <Card>
        <CardHeader title={t("register")} />
        <Divider></Divider>
        <CardContent>
          <form onSubmit={handleSubmit(registerUser)}>
            <Field>
              <TextField
                label={t("username")}
                variant="standard"
                {...register("username", { required: "asdf" })}
              />
              <Error>{errors?.username?.message}</Error>
            </Field>

            <div className="md:flex gap-4">
              <Field>
                <TextField
                  variant="standard"
                  label={t("password")}
                  type="password"
                  className={`${errors.password ? "border border-red-400" : ""
                    }`}
                  {...register("password")}
                />
                <Error>{errors?.password?.message}</Error>
              </Field>

              <Field>
                <TextField
                  variant="standard"
                  type="password"
                  label={t("passwordConfirm")}
                  {...register("passwordConfirmation")}
                />
                <Error>{errors?.passwordConfirmation?.message}</Error>
              </Field>
            </div>

            <Field>
              <div>{t("hobbies")}</div>
              <div className="flex flex-wrap gap-2">
                {hobbies.map((val) => (
                  <label
                    key={val}
                    className="border border-gray-400 rounded flex items-center gap-1 pl-3"
                  >
                    {val}
                    <Checkbox value={val} {...register("hobbies")}></Checkbox>
                  </label>
                ))}
              </div>
              <Error>{errors?.hobbies?.message}</Error>
            </Field>

            <Field>
              <TextField
                type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                label={t("birthDate")}
                {...register("birthDate")}
              />
              <Error>{errors?.birthDate?.message}</Error>
            </Field>

            <div className="flex space-x-3">
              <Button type="submit" variant="contained" color="info">
                register
              </Button>

              {loggedIn && (
                <div className="space-x-2">
                  <Button variant="outlined" color="secondary">
                    <Link to={"/"}>go home</Link>
                  </Button>
                </div>
              )}
            </div>
          </form>
        </CardContent>
      </Card>
      {Object.keys(state).length ? (
        <Card>
          <CardHeader title="Form value" />
          <Divider></Divider>
          <CardContent>
            <pre>{JSON.stringify(state, null, 2)}</pre>
          </CardContent>
        </Card>
      ) : (
        ""
      )}
    </Box>
  );
}
