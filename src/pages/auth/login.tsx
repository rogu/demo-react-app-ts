import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Controller, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { useCoreCtx } from "../../core/ctx/core-ctx";

const useStyles = makeStyles({
  root: {
    display: "flex",
    flexDirection: "column",
    gap: 40,
    marginTop: 20,
  },
});

export const Login = () => {
  const { state, logIn, logOut } = useCoreCtx();
  const { handleSubmit, control } = useForm();
  const classes = useStyles();

  return (
    <Card sx={{ width: { sm: "50%", xs: "100%" } }}>
      <CardHeader title="Login form" />
      <Divider></Divider>
      <CardContent>
        {state.loggedIn ? (
          <div className="space-x-2">
            <Box sx={{ marginBottom: 2 }}>You are logged in</Box>
            <Button variant="outlined" color="secondary" onClick={logOut}>
              log out
            </Button>
            <Button variant="outlined" color="secondary" >
              <Link to={"/"}>home</Link>
            </Button>
          </div>
        ) : (
          <form onSubmit={handleSubmit(logIn)} className={classes.root}>
            <Controller
              name="username"
              control={control}
              defaultValue="admin@localhost"
              rules={{ required: "Username required" }}
              render={({ field: { onChange, value } }) => (
                <TextField
                  fullWidth
                  label="Username"
                  variant="outlined"
                  value={value}
                  size="small"
                  onChange={onChange}
                />
              )}
            />
            <Controller
              name="password"
              control={control}
              defaultValue="Admin1"
              rules={{ required: "Password required" }}
              render={({ field: { onChange, value } }) => (
                <TextField
                  fullWidth
                  label="Password"
                  type="password"
                  variant="outlined"
                  value={value}
                  size="small"
                  onChange={onChange}
                />
              )}
            />
            <div className="space-x-2">
              <Button
                type="submit"
                size="small"
                variant="contained"
                color="primary"
              >
                log&nbsp;in
              </Button>
              <Link to={"/auth/1"}>
                <Typography variant="button" color="initial">
                  or Register
                </Typography>
              </Link>
            </div>
          </form>
        )}
      </CardContent>
    </Card>
  );
};
