import { Alert, Container } from "@mui/material";
import { useCallback } from "react";
import { useNavigate, useParams } from "react-router-dom";
import TabBox from "../../common/ui/tab-box";
import { Login } from "./login";
import { Register } from "./register";

const tabs = ["login", "register"];

export const Auth = () => {
  const navi = useNavigate();
  const { id } = useParams();

  const actionHandler = useCallback((ev: any) => navi(`/auth/${ev}`), [navi]);

  return (
    <Container sx={{ padding: 2 }}>
      <Alert severity="info">Przykład użycia: react context, useForm, yup validation, makeStyles ...</Alert>
      <TabBox tabs={tabs} selected={Number(id)} onAction={actionHandler}>
        <Login></Login>
        <Register></Register>
      </TabBox>
    </Container>
  );
};
export default Auth;
