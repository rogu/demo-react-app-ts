import axios from "axios";
import { useInfiniteQuery } from "@tanstack/react-query";
import { createContext, useContext, useEffect } from "react";
import { Api } from "../../common/utils/api";
import { useInView } from 'react-intersection-observer';

export const InfinityCtx = createContext({} as any);

export const InfinityCtxProvider = ({ children }: any) => {
    const { ref, inView } = useInView();
    const { data, fetchNextPage } = useInfiniteQuery({
        queryKey: ['infinity'],
        queryFn: ({ pageParam = 1 }) => axios.get(`${Api.CATS_END_POINT}?itemsPerPage=5&currentPage=${pageParam}`),
        getNextPageParam: (lastPage: any) => lastPage.hasMore
            ? +lastPage.currentPage + 1
            : undefined
    });

    useEffect(() => {
        inView && fetchNextPage();
    }, [inView, fetchNextPage]);

    return <InfinityCtx.Provider value={{ data, ref }}>
        {children}
    </InfinityCtx.Provider>;
};

export const useInfinityCtx = () => useContext(InfinityCtx);
