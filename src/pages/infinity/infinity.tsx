import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import { Alert, Card, CardActions, CardContent, CardMedia, IconButton } from "@mui/material";
import { FunctionComponent } from "react";
import { useInfinityCtx } from "./infinity.ctx";

interface InfinityProps { }

const InfinityList: FunctionComponent<InfinityProps> = () => {
    const ctx = useInfinityCtx();
    return (<>
        <Alert severity="info">Przykład użycia: react context & react query + useInfiniteQuery ...</Alert>
        {ctx.data?.pages.map((page: any, i1: number) => <div key={i1}>
            {page.data.map((item: any, i2: number) => <Card className="m-10 lg:mx-40 !bg-blue-100" key={item.name} >
                <CardMedia
                    sx={{ height: 250 }}
                    image={item.imgSrc}
                    title={item.name}
                />
                <CardContent className='font-bold text-xl'>
                    {item.name} {i1 * 5 + i2}
                </CardContent>
                <CardActions>
                    <IconButton aria-label="add to favorites">
                        <FavoriteIcon />
                    </IconButton>
                    <IconButton aria-label="share">
                        <ShareIcon />
                    </IconButton>
                </CardActions>
            </Card>)}
        </div>)}
        <button ref={ctx.ref}>next</button>
    </>);
};

export default InfinityList;
