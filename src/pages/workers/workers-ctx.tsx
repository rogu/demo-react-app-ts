import axios from "axios";
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useReducer,
  useState,
} from "react";
import { CreateAction, HttpResponseModel } from "../../common/types/models";
import { Api } from "../../common/utils/api";
import workersReducer, {
  WorkersActions,
  WorkersState,
} from "./workers-reducer";

export interface WorkersCtxModel {
  state: WorkersState;
  searchWorkers: Function;
  removeWorker: Function;
  addWorker: Function;
}

export const WorkersCtx = createContext({} as WorkersCtxModel);

export const WorkersCtxProvider = ({ children }: any) => {
  const [state, dispatch] = useReducer(workersReducer, {
    total: 0,
    filteredWorkers: [],
    workersBackup: [],
    loading: false,
  } as WorkersState);
  const [filters, setFilters] = useState({});

  useEffect(() => {
    dispatch(new CreateAction(WorkersActions.SEARCH_WORKERS, filters));
  }, [filters]);

  const fetchWorkers = useCallback(async () => {
    dispatch(new CreateAction(WorkersActions.LOADING));
    const { data } = await axios.get(Api.WORKERS_END_POINT);
    dispatch(new CreateAction(WorkersActions.SAVE_WORKERS, data));
  }, []);

  const searchWorkers = useCallback(
    (filters: any) => setFilters((prev) => ({ ...prev, ...filters })),
    []
  );

  const removeWorker = useCallback(async (id: string) => {
    await axios.delete(`${Api.WORKERS_END_POINT}/${id}`);
    dispatch(new CreateAction(WorkersActions.REMOVE_WORKER, id));
  }, []);

  const addWorker = useCallback(async (worker: any) => {
    const { data } = await axios.post<any, HttpResponseModel>(Api.WORKERS_END_POINT, worker);
    dispatch(new CreateAction(WorkersActions.ADD_WORKER, data));
    return true;
  }, []);

  useEffect(() => {
    fetchWorkers();
  }, [fetchWorkers]);

  return (
    <WorkersCtx.Provider
      value={{
        state,
        searchWorkers,
        removeWorker,
        addWorker,
      }}
    >
      {children}
    </WorkersCtx.Provider>
  );
};

export const useWorkersCtx = () => useContext(WorkersCtx);
