import { Button, FormControl, MenuItem, TextField } from "@mui/material";
import { Box } from "@mui/system";
import { FunctionComponent } from "react";
import { useForm } from "react-hook-form";
import { Action } from "../../common/types/models";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import Error from "../../common/ui/error";

interface AddWorkerFormProps {
  onAction: (a: Action) => void;
}

const AddWorkerForm: FunctionComponent<AddWorkerFormProps> = ({ onAction }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(
      yup.object({
        name: yup.string().required(),
        phone: yup.number().required(),
        category: yup
          .mixed()
          .oneOf(["support", "sales"], "only support or sales"),
      })
    ),
  });

  const clickHandler = (payload: any) => {
    onAction({ type: "ADD", payload });
  };
  return (
    <form onSubmit={handleSubmit(clickHandler)}>
      <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
        <FormControl>
          <TextField
            autoFocus
            size="small"
            label="name"
            {...register("name")}
          />
          <Error>{errors.name?.message}</Error>
        </FormControl>
        <FormControl>
          <TextField
            size="small"
            label="phone"
            type="number"
            {...register("phone", { required: "pole wymagane" })}
          />
          <Error>{errors.phone?.message}</Error>
        </FormControl>
        <FormControl>
          <TextField
            select
            fullWidth
            label="category"
            size="small"
            defaultValue={""}
            {...register("category")}
          >
            <MenuItem value="sales">sales</MenuItem>
            <MenuItem value="support">support</MenuItem>
          </TextField>
          <Error>{errors.category?.message}</Error>
        </FormControl>
        <hr />
        <Button type="submit" variant="contained">
          send
        </Button>
      </Box>
    </form>
  );
};

export default AddWorkerForm;
