import { Alert, Box, Button, Card, CardHeader, Chip, Divider } from "@mui/material";
import {
  FunctionComponent,
  useCallback,
  useMemo,
  useRef,
} from "react";
import { DataGrid } from "../../common/ui/data-grid";
import SimpleModal from "../../common/ui/modal";
import { Search } from "../../common/ui/search";
import {
  Action,
  GridFieldModel,
  SearchControlModel,
  WorkersFiltersKeys,
  WorkersGridKeys,
} from "../../common/types/models";
import AddWorkerForm from "./add-worker-form";
import { useWorkersCtx } from "./workers-ctx";

interface WorkersProps { }

const Workers: FunctionComponent<WorkersProps> = () => {
  const ctx = useWorkersCtx();
  const addWorkerModal: any = useRef();

  const controls: SearchControlModel<WorkersFiltersKeys>[] = useMemo(
    () => [
      { key: "name", type: "input" },
      { key: "phone", type: "input" },
      { key: "category", type: "select", options: ["support", "sales"] },
    ],
    []
  );

  const gridConfig: GridFieldModel<WorkersGridKeys>[] = useMemo(
    () => [
      { key: "name" },
      { key: "phone", type: "input" },
      { key: "category" },
    ],
    []
  );

  const actionHandler = useCallback(
    async ({ type, payload }: Action) => {
      switch (type) {
        case "REMOVE":
          window.confirm("are you sure?") && ctx.removeWorker(payload);
          break;
        case "ADD":
          await ctx.addWorker(payload);
          addWorkerModal.current?.handleClose();
          break;
        case "search":
          ctx.searchWorkers(payload);
          break;
      }
    },
    [ctx]
  );

  return (
    <>
      <Alert severity="info">Przykład użycia: react context, useReducer ...</Alert>
      <div className="md:flex gap-4 space-y-3 md:space-y-0 my-4">
        <div className="basis-1/4">
          <Search onAction={actionHandler} controls={controls}></Search>
        </div>
        <div className="basis-3/4">
          <div>{ctx.state.loading && "loading..."}</div>
          <Card>
            <CardHeader title="Workers list" />
            <Divider></Divider>
            <Box
              sx={{
                ml: 2,
                m: 2,
                display: "flex",
                gap: 2,
                opacity: ctx.state.filteredWorkers.length ? 1 : 0.5,
              }}
            >
              <SimpleModal text="add worker" ref={addWorkerModal}>
                <AddWorkerForm onAction={actionHandler}></AddWorkerForm>
              </SimpleModal>
              <Chip
                label={`workers: ${ctx.state.filteredWorkers.length}/${ctx.state.total}`}
              />
            </Box>
            <DataGrid
              data={ctx.state.filteredWorkers}
              config={gridConfig}
              onAction={actionHandler}
            >
              <Button variant="contained" size="small" color="info">
                remove
              </Button>
            </DataGrid>
          </Card>
        </div>
      </div>
    </>
  );
};

export default Workers;
