import { Action, WorkerModel } from "../../common/types/models";
import { search } from "../../common/utils/helpers";

export enum WorkersActions {
  LOADING = "LOADING_WORKERS",
  SAVE_WORKERS = "SAVE_WORKERS",
  SEARCH_WORKERS = "SEARCH_WORKERS",
  ADD_WORKER = "ADD_WORKERS",
  REMOVE_WORKER = "REMOVE_WORKERS",
}

export interface WorkersState {
  filteredWorkers: WorkerModel[];
  workersBackup: WorkerModel[];
  total: number;
  loading: boolean;
}

export default function reducer(
  state: WorkersState,
  { type, payload }: Action
): WorkersState {
  let filteredWorkers;
  switch (type) {
    case WorkersActions.LOADING:
      return { ...state, loading: true };
    case WorkersActions.SAVE_WORKERS:
      return {
        ...state,
        loading: false,
        filteredWorkers: payload,
        workersBackup: payload,
        total: payload.length,
      };
    case WorkersActions.SEARCH_WORKERS:
      filteredWorkers = search(state.workersBackup, payload);
      return { ...state, filteredWorkers };
    case WorkersActions.REMOVE_WORKER:
      filteredWorkers = state.filteredWorkers.filter(
        (t: any) => t.id !== payload
      );
      return {
        ...state,
        filteredWorkers,
        workersBackup: filteredWorkers,
        total: filteredWorkers.length,
      };
    case WorkersActions.ADD_WORKER:
      filteredWorkers = [...state.filteredWorkers, payload];
      return {
        ...state,
        filteredWorkers,
        workersBackup: filteredWorkers,
        total: filteredWorkers.length,
      };
    default:
      return state;
  }
}
