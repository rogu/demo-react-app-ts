import {
  Alert,
  Box,
  Button,
  Card,
  CardHeader,
  Chip,
  Divider,
  Pagination,
} from "@mui/material";
import { useCallback, useMemo, useRef } from "react";
import { useNavigate } from "react-router-dom";
import {
  Action,
  GridFieldModel,
  ItemsFiltersKeys,
  ItemsGridKeys,
  SearchControlModel,
} from "../../common/types/models";
import { DataGrid } from "../../common/ui/data-grid";
import SimpleModal from "../../common/ui/modal";
import { Search } from "../../common/ui/search";
import { AddItemForm } from "./add-item-form";
import { useItemsCtx } from "./items-ctx";

export function Items({ loggedIn }: { loggedIn: boolean; }) {
  const { data, total, filters, updateFilters, remove, add } = useItemsCtx();
  const gridConfig: GridFieldModel<ItemsGridKeys>[] = useMemo(
    () => [
      { key: "title" },
      { key: "price" },
      { key: "imgSrc", type: "image" },
    ],
    []
  );
  const navigate = useNavigate();

  const addItemModal: any = useRef();

  const actionHandler = useCallback(
    async ({ type, payload }: Action) => {
      switch (type.toLowerCase()) {
        case "more":
          navigate(payload);
          break;
        case "remove":
          window.confirm("are you sure?") && remove(payload);
          break;
        case "search":
          updateFilters(payload);
          break;
        case "add":
          await add(payload);
          addItemModal.current?.handleClose();
          break;
      }
    },
    [updateFilters, remove, add, navigate]
  );

  const setPage = useCallback(
    (ev: any, currentPage: number) => {
      updateFilters({ currentPage });
    },
    [updateFilters]
  );

  const searchConfig: SearchControlModel<ItemsFiltersKeys>[] = useMemo(
    () => [
      { key: "title", type: "input" },
      { key: "priceFrom", type: "slider", defaultValue: 0 },
      {
        key: "category",
        type: "select",
        options: ["food", "clothes"],
      },
      {
        key: "itemsPerPage",
        type: "select",
        options: [2, 5, 10],
        defaultValue: 5,
      },
    ],
    []
  );

  return (
    <>
    <Alert severity="info">Przykład użycia: react context, useState, RESTful CRUD, paginacja, modal, wyszukiwarka ...</Alert>
      <div className="md:flex gap-4 space-y-3 md:space-y-0 my-4">
        <div className="basis-1/4">
          <Search controls={searchConfig} onAction={actionHandler}></Search>
        </div>
        <Card sx={{ flexGrow: 1, flexBasis: "2/4" }}>
          <CardHeader title="Items list" />
          <Divider></Divider>
          <Box
            sx={{
              m: 2,
              display: "flex",
              flexDirection: { xs: "column", sm: "row" },
              justifyContent: "space-between",
              opacity: data.length ? 1 : 0.5,
            }}
          >
            <Box sx={{ display: "flex", gap: 2 }}>
              <SimpleModal ref={addItemModal} text="add item">
                <AddItemForm onAction={actionHandler}></AddItemForm>
              </SimpleModal>
              <Chip label={"total: " + total} />
            </Box>
            <Pagination
              page={filters.currentPage}
              onChange={setPage}
              count={Math.ceil(total / +filters.itemsPerPage)}
              color="primary"
            />
          </Box>
          <DataGrid data={data} config={gridConfig} onAction={actionHandler}>
            <Button variant="outlined" size="small">
              more
            </Button>
            <Button disabled={!loggedIn} variant="contained" size="small">
              remove
            </Button>
          </DataGrid>
          <Box
            sx={{
              m: 2,
              display: "flex",
              justifyContent: "end",
            }}
          >
            <Pagination
              page={filters.currentPage}
              onChange={setPage}
              count={Math.ceil(total / +filters.itemsPerPage)}
              color="primary"
            />
          </Box>
        </Card>
      </div>
    </>
  );
}
