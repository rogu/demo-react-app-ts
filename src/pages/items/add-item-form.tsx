import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Button, MenuItem, TextField, FormControl } from "@mui/material";
import { useForm } from "react-hook-form";
import { Action } from "../../common/types/models";
import Error from "../../common/ui/error";

interface AddItemModel {
  onAction: (a: Action) => void;
}

export const AddItemForm = ({ onAction }: AddItemModel) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(
      yup.object({
        title: yup.string().required().min(5),
        price: yup.number().required().min(5),
        category: yup
          .mixed()
          .oneOf(["food", "clothes"], "only food or clothes"),
      })
    ),
  });

  const onSubmit = (payload: any) => {
    onAction({ type: "add", payload });
  };

  return (
    <form className="space-y-5" onSubmit={handleSubmit(onSubmit)}>
      <FormControl fullWidth>
        <TextField
          label="Title"
          variant="outlined"
          autoFocus
          size="small"
          {...register("title")}
        />
        <Error>{errors.title?.message}</Error>
      </FormControl>
      <FormControl fullWidth>
        <TextField
          label="Price"
          variant="outlined"
          fullWidth
          size="small"
          {...register("price")}
        />
        <Error>{errors.price?.message}</Error>
      </FormControl>
      <FormControl fullWidth>
        <TextField
          select
          label="category"
          size="small"
          defaultValue={""}
          {...register("category")}
        >
          <MenuItem value="food">food</MenuItem>
          <MenuItem value="clothes">clothes</MenuItem>
        </TextField>
        <Error>{errors.category?.message}</Error>
      </FormControl>

      <Button variant="contained" type="submit">
        send
      </Button>
    </form>
  );
};
