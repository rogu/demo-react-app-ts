import { useDebounce } from "@react-hook/debounce";
import axios from "axios";
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import {
  CoreStateAction,
  CreateAction,
  CtxModel,
  HttpResponseModel,
  ItemModel,
  ItemsFiltersModel,
} from "../../common/types/models";
import { Api } from "../../common/utils/api";
import { useCoreCtx } from "../../core/ctx/core-ctx";

export interface ItemsCtxModel extends CtxModel<ItemModel[]> {
  updateFilters: Function;
  filters: ItemsFiltersModel;
}

export const ItemsCtx = createContext({} as ItemsCtxModel);

export const ItemsCtxProvider = ({ children }: any) => {
  const [data, setData] = useState<ItemModel[]>([]);
  const [total, setTotal] = useState<number>(0);
  const [filters, setFilters] = useDebounce(
    {
      itemsPerPage: 5,
      currentPage: 1,
    } as ItemsFiltersModel,
    500
  );
  const coreCtx = useCoreCtx();

  const fetchItems = useCallback(async () => {
    const { data, total } = await axios.get<any, HttpResponseModel>(
      `${Api.ITEMS_END_POINT}`,
      { params: filters }
    );
    setData(data);
    setTotal(total || 0);
  }, [filters]);

  const updateFilters = useCallback(
    (data: any) => {
      coreCtx.dispatch(new CreateAction(CoreStateAction.SET_LOADING, true));
      setFilters((prev: any) => ({
        ...prev,
        ...data,
        ...(!("currentPage" in data) && { currentPage: 1 }),
      }));
    },
    [setFilters, coreCtx]
  );

  const remove = useCallback(
    async (id: string) => {
      await axios.delete(`${Api.ITEMS_END_POINT}/${id}`);
      fetchItems();
    },
    [fetchItems]
  );

  const add = useCallback(
    async (item: ItemModel) => {
      const resp = await axios.post(Api.ITEMS_END_POINT, item);
      fetchItems();
      return resp;
    },
    [fetchItems]
  );

  useEffect(() => {
    fetchItems();
  }, [fetchItems]);

  return (
    <ItemsCtx.Provider
      value={{ total, data, filters, updateFilters, remove, add }}
    >
      {children}
    </ItemsCtx.Provider>
  );
};

export const useItemsCtx = () => useContext(ItemsCtx);
