import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Typography,
} from "@mui/material";
import { FunctionComponent, useContext } from "react";
import { useParams } from "react-router-dom";
import { ItemModel } from "../../common/types/models";
import { ItemsCtx } from "./items-ctx";

interface ItemProps {}

const Item: FunctionComponent<ItemProps> = () => {
  const { data: items } = useContext(ItemsCtx);
  const { id } = useParams();
  const item = items.find((item: ItemModel) => item.id === id);

  return (
    <Card>
      <CardMedia component="img" height="140" image={item?.imgSrc} />
      <CardHeader title="Item details" />
      <CardContent>
        {item ? (
          <>
            <Typography gutterBottom variant="h5" component="div">
              title: {item?.title}
            </Typography>
            <Typography variant="body1" color="text.secondary">
              price: {item?.price}
            </Typography>
            <Typography variant="body2" color="text.info">
              category: {item?.category}
            </Typography>
          </>
        ) : (
          <div>
            nothing to show <br /> click more button to see details
          </div>
        )}
      </CardContent>
    </Card>
  );
};

export default Item;
