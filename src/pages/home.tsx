import { Alert, MenuItem, TextField } from "@mui/material";
import { useTranslation } from "react-i18next";
import { CoreStateAction, CreateAction } from "../common/types/models";
import { useCoreCtx } from "../core/ctx/core-ctx";

export function Home() {
  const { t, i18n } = useTranslation();
  const { state, dispatch } = useCoreCtx();
  const handleToggleLang = (ev: any) =>
    dispatch(new CreateAction(CoreStateAction.SET_LANG, ev.target.value));
  const langs = Object.keys(i18n.services.resourceStore.data);

  return (
    <div className="mt-8 space-y-10">
      <div>
        <div className="flex gap-4 mb-4">
          <Alert severity="warning">
            Aplikacja jest uproszczoną formą kodu do analizy. <br />
            Nie zawiera opisów. <br />
          </Alert>
          <Alert severity="info">
            Przykład użycia react-i18next
          </Alert>
        </div>
        <div className="font-bold text-5xl">{t("welcome")}</div>
        <div className="font-bold text-gray-500 text-2xl">{t("title")}</div>
      </div>
      <TextField
        select
        label="Language"
        size="small"
        value={state.lang}
        onChange={handleToggleLang}
        helperText="Please select your language"
      >
        {langs.map((opt) => (
          <MenuItem key={opt} value={opt}>
            {opt}
          </MenuItem>
        ))}
      </TextField>
    </div>
  );
}
