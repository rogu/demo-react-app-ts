import axios, { AxiosResponse } from "axios";
import { createContext, useEffect, useState } from "react";
import { HttpResponseModel } from "../../common/types/models";
import { Api } from "../../common/utils/api";

export interface ProfileModel {
  state: any;
}

export const ProfileCtx = createContext({} as ProfileModel);

export const ProfileProvider = ({ children }: any) => {
  const [state, setState] = useState<any[]>([]);

  useEffect(() => {
    axios
      .get(Api.PROFILE_END_POINT)
      .then(({ data }: AxiosResponse<HttpResponseModel>) => setState(Object.entries(data)));
  }, []);

  return (
    <ProfileCtx.Provider value={{ state }}>{children}</ProfileCtx.Provider>
  );
};
