import { Card, CardContent } from "@mui/material";
import { FunctionComponent, useContext } from "react";
import { ProfileCtx } from "./profile-ctx";

interface ProfileProps { }

const Profile: FunctionComponent<ProfileProps> = () => {
  const ctx = useContext(ProfileCtx);

  return (
    <Card>
      <CardContent>
        <div className="divide-y-2 space-y-2">
          <div className="text-2xl">Your profile data</div>
          <ul className="pt-2">
            {ctx.state?.map(([key, value]: any[]) => (
              <li>{key} : {value}</li>
            ))}
          </ul>
        </div>
      </CardContent>
    </Card>
  );
};

export default Profile;
