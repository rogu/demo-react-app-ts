import axios from "axios";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { createContext, useContext, useState } from "react";
import { CatModel, CtxModel, HttpResponseModel } from "../../common/types/models";
import { Api } from "../../common/utils/api";

export interface CatsModel extends CtxModel<CatModel[]> {
    setPrev: () => void;
    setNext: () => void;
    page: number;
    hasMore: boolean | undefined;
}

export const CatsCtx = createContext({} as CatsModel);

export const CatsCtxProvider = ({ children }: any) => {
    const queryClient = useQueryClient();
    const [page, setPage] = useState(1);
    const { data: { data, total, hasMore }, isLoading } = useQuery({
        queryKey: ['cats', page],
        queryFn: async ({ queryKey: [, page] }) =>
            await axios.get<any, HttpResponseModel>(`${Api.CATS_END_POINT}?currentPage=${page}&itemsPerPage=5`),
        initialData: { data: [], total: 0, },
        keepPreviousData: true
    });
    const remove: any = useMutation({
        mutationFn: (id: string) => axios.delete(`${Api.CATS_END_POINT}/${id}`),
        onSuccess: () => queryClient.invalidateQueries({ queryKey: ['cats'] })
    });
    const add: any = useMutation({
        mutationFn: (data: CatModel) => axios.post(Api.CATS_END_POINT, data),
        onSuccess: () => queryClient.invalidateQueries({ queryKey: ['cats'] })
    });
    const setPrev = () => setPage((old: number) => Math.max(old - 1, 1));
    const setNext = () => setPage((old: number) => old + 1);

    if (isLoading) return <div>loading...</div>;
    return <CatsCtx.Provider value={{ data, total, page, hasMore, add, remove, setPrev, setNext }}>{children}</CatsCtx.Provider>;
};
export const useCatsCtx = () => useContext(CatsCtx);

