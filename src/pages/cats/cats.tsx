import { Alert, Button, Chip, TextField } from "@mui/material";
import { FunctionComponent, useCallback, useMemo, useRef } from "react";
import { Action, CatsGridKeys, GridFieldModel } from "../../common/types/models";
import { DataGrid } from "../../common/ui/data-grid";
import SimpleModal from "../../common/ui/modal";
import { useCatsCtx } from "./cats.ctx";

interface CatsProps { }

const Cats: FunctionComponent<CatsProps> = () => {
    const ctx = useCatsCtx();
    const gridConfig: GridFieldModel<CatsGridKeys>[] = useMemo(
        () => [
            { key: "name" },
            { key: "imgSrc", type: "image" },
        ],
        []
    );

    const gridActionHandler = useCallback(
        async ({ type, payload }: Action) => {
            switch (type.toLowerCase()) {
                case "remove":
                    window.confirm("are you sure?") && ctx.remove.mutate(payload);
                    break;
            }
        },
        [ctx.remove]
    );

    const create = useCallback((ev: any) => {
        ev.preventDefault();
        ctx.add.mutate({ name: ev.target.name.value });
    }, [ctx.add]);

    const addItemModal: any = useRef();
    return (<>
        <Alert severity="info">Przykład użycia: react context & react query + pagination ...</Alert>
        <div className="flex items-center gap-2 my-4">
            <div className="flex-grow">
                <SimpleModal ref={addItemModal} text="add cat">
                    <form onSubmit={create} className="flex flex-col gap-4">
                        <TextField autoFocus size="small" name="name" required></TextField>
                        <div><Button variant="contained" type="submit">send</Button></div>
                    </form>
                </SimpleModal>
            </div>
            <Chip label={`cats total: ${ctx.total}`}/>
            <Chip label={`page: ${ctx.page}`} />
            <Button variant="contained" size="small" disabled={!(ctx.page - 1)} onClick={ctx.setPrev}>prev</Button>
            <Button variant="contained" size="small" disabled={!ctx.hasMore} onClick={ctx.setNext}>next</Button>
        </div>
        <DataGrid data={ctx.data} config={gridConfig} onAction={gridActionHandler}>
            <Button variant="contained" size="small">
                remove
            </Button>
        </DataGrid>
    </>);
};

export default Cats;
