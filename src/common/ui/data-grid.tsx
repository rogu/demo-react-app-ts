import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@mui/material";
import { Box } from "@mui/system";
import { ReactNode } from "react";
import { GridFieldModel } from "../types/models";

interface GridProps {
  data: any[];
  config: GridFieldModel<any>[];
  children: ReactNode;
  onAction: Function;
}

export const DataGrid = ({ data, config, children, onAction }: GridProps) => {
  function action({
    target: { innerText: type },
    currentTarget: {
      dataset: { id: payload },
    },
  }: any) {
    onAction({ type, payload });
  }

  return (
    <>
      <TableContainer>
        <Table>
          <TableHead className="bg-gray-100">
            <TableRow>
              <TableCell>Lp.</TableCell>
              {config.map((prop, idx) => (
                <TableCell key={idx}>{prop.key.toUpperCase()}</TableCell>
              ))}
              {children && (
                <TableCell sx={{ textAlign: "center" }}>ACTIONS</TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.map((item: any, idx) => (
              <TableRow key={item.id}>
                <TableCell>{idx + 1}</TableCell>
                {config.map((prop: GridFieldModel<any>, idx) => {
                  switch (prop.type) {
                    case "image":
                      return (
                        <TableCell key={idx}>
                          <img src={item[prop.key]} alt="obrazek" width={55} />
                        </TableCell>
                      );
                    case "input":
                      return (
                        <TableCell key={idx}>
                          <TextField
                            size="small"
                            defaultValue={item[prop.key]}
                          />
                        </TableCell>
                      );
                    default:
                      return <TableCell key={idx}>{item[prop.key]}</TableCell>;
                  }
                })}
                <TableCell onClick={action} data-id={item.id}>
                  <Box
                    sx={{ display: "flex", gap: 2, justifyContent: "center" }}
                  >
                    {children}
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};
