import { CircularProgress, Box } from "@mui/material";

export default function Spinner() {
  return (
    <>
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          height: "100vh",
          zIndex: 100,
          transition: "background-color 200ms linear",
          display: "flex",
        }}
      >
        <div className="mx-auto mt-3">
          <CircularProgress size={40} color="info" />
        </div>
      </Box>
    </>
  );
}
