import { forwardRef, useImperativeHandle, useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import { Divider, Typography } from "@mui/material";

interface ModalProps {
  text: string;
  children: any;
}

export const SimpleModal = forwardRef<any, ModalProps>(
  ({ text, children }: any, ref) => {
    useImperativeHandle(ref, () => ({ handleClose }));
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
      <div>
        <Button variant="contained" size="small" onClick={handleOpen}>
          {text}
        </Button>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              width: 400,
              bgcolor: "background.paper",
              border: "2px solid #000",
              boxShadow: 24,
            }}
          >
            <Typography sx={{ p: 2 }} variant="body1">
              {text.toUpperCase()}
            </Typography>
            <Divider></Divider>
            <Box sx={{ p: 2 }}>{children}</Box>
          </Box>
        </Modal>
      </div>
    );
  }
);

export default SimpleModal;
