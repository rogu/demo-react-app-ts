import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  MenuItem,
  Slider,
  TextField,
} from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { useForm } from "react-hook-form";
import { Action, SearchControlModel } from "../types/models";
import { CardHeader } from "@mui/material";

interface SearchModel {
  controls: SearchControlModel<any>[];
  onAction: (a: Action) => void;
}

export const Search = ({ controls, onAction }: SearchModel) => {
  const initState = useMemo(
    () =>
      controls.reduce(
        (acc: any, control: SearchControlModel<any>) => ({
          ...acc,
          [control.key]: isNaN(control.defaultValue)
            ? control.defaultValue || ""
            : control.defaultValue,
        }),
        {}
      ),
    [controls]
  );
  const { register, watch, handleSubmit, reset } = useForm();
  const [state, setState] = useState(initState);
  const onSubmit = () => reset(initState);

  useEffect(() => {
    watch((payload) => {
      setState(payload);
      onAction({ type: "search", payload });
    });
  }, [watch, setState, onAction]);

  return (
    <Card>
      <form onSubmit={handleSubmit(onSubmit)}>
        <CardHeader title="Search" />
        <Divider></Divider>
        <CardContent>
          <div className="space-y-5">
            {controls.map((control: SearchControlModel<any>) => {
              switch (control.type) {
                case "input":
                  return (
                    <TextField
                      key={control.key}
                      fullWidth
                      size="small"
                      label={control.key}
                      value={state[control.key]}
                      {...register(control.key)}
                    />
                  );
                case "slider":
                  return (
                    <Box key={control.key} sx={{ px: 1 }}>
                      {control.key}
                      <Slider
                        value={+state[control.key]}
                        aria-label="Default"
                        valueLabelDisplay="auto"
                        {...(register(control.key) as any)}
                      />
                    </Box>
                  );
                case "select":
                  return (
                    <TextField
                      fullWidth
                      key={control.key}
                      value={state[control.key]}
                      select
                      label={control.key}
                      size="small"
                      {...register(control.key)}
                    >
                      {control.options?.map((val, idx) => (
                        <MenuItem key={idx} value={val}>
                          {val}
                        </MenuItem>
                      ))}
                    </TextField>
                  );
                default:
                  return <div key={control.key}>control not found</div>;
              }
            })}
          </div>
        </CardContent>
        <CardActions>
          <Button size="small" type="submit">
            clear
          </Button>
        </CardActions>
      </form>
    </Card>
  );
};
