import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import { ReactNode, SyntheticEvent, useEffect, useState } from "react";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

interface TabBoxProps {
  tabs: string[];
  children: ReactNode[];
  selected?: number;
  onAction?: Function;
}

function TabPanel({ children, value, index }: TabPanelProps) {
  return (
    <div role="tabpanel" hidden={value !== index}>
      <Box sx={{ mt: 2 }}>{children}</Box>
    </div>
  );
}

export default function TabBox({
  tabs,
  children,
  selected,
  onAction,
}: TabBoxProps) {
  const [current, setCurrent] = useState(selected || 0);
  useEffect(() => {
    setCurrent(selected || 0);
  }, [selected]);

  const handleChange = (ev: SyntheticEvent, newValue: number) => {
    setCurrent(newValue);
    onAction && onAction(newValue);
  };

  return (
    <Box>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs value={current} onChange={handleChange}>
          {tabs.map((v: any) => (
            <Tab key={v} label={v} />
          ))}
        </Tabs>
      </Box>

      {children.map((v: any, i: any) => (
        <TabPanel key={i} value={current} index={i}>
          {v}
        </TabPanel>
      ))}
    </Box>
  );
}
