import { Box } from "@mui/material";
const Error = ({ children }: any) => {
    return <Box sx={{ color: 'red' }}>
        {children}
    </Box>;
};
export default Error;
