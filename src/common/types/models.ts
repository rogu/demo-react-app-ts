/**
 * Context
 */

export interface CtxModel<T> {
    add: any;
    remove: any;
    data: T;
    total: number;
}

/**
 * Http
 */
export interface HttpResponseModel {
    data: any;
    total: number;
    hasMore?: boolean;
    message?: string;
    error?: string;
    warning?: string;
    accessToken?: string;
    refreshToken?: string;
}

/**
 * Auth
 */
export interface AuthDataModel {
    username: string;
    password: string;
}

/**
 * Core
 */
export enum CoreStateAction {
    SET_LOADING = 'CORE SET LOADING',
    SET_THEME = 'CORE SET THEME',
    SET_LOGGEDIN = 'CORE SET LOGGEDIN',
    SET_LANG = 'CORE SET LANG'
}

export interface CoreStateModel {
    loading: boolean;
    theme: string;
    loggedIn: boolean | undefined;
    lang: string;
    version: string;
}


/**
 * Items
 */
export interface ItemModel {
    id?: string;
    category: string;
    imgSrc: string;
    price: number;
    title: string;
}

export interface ItemsFiltersModel {
    title?: string;
    priceFrom?: number;
    category?: string;
    currentPage: number;
    itemsPerPage: number;
}

export type ItemsFiltersKeys = 'title' | 'priceFrom' | 'category' | 'currentPage' | 'itemsPerPage';
export type ItemsGridKeys = 'title' | 'imgSrc' | 'price';

/**
 * Workers
 */
export interface WorkerModel {
    id?: string,
    name: string,
    phone: number,
    category: string;
}

export type WorkersFiltersKeys = 'name' | 'phone' | 'category';
export type WorkersGridKeys = 'name' | 'phone' | 'category';

/**
 * Cats
 */
export interface CatModel {
    id?: string,
    name: string,
    imgSrc: string;
}

export type CatsFiltersKeys = 'name';
export type CatsGridKeys = 'name' | 'imgSrc';

/**
 * Data grid component
 */
export type GridFieldTypes = 'image' | 'input' | 'button';

export interface GridFieldModel<T> {
    key?: T;
    type?: GridFieldTypes;
    text?: string;
}

/**
 * Search component
 */
export type ControlTypes = 'input' | 'select' | 'button' | 'slider';

export interface SearchControlModel<T> {
    key: T;
    type: ControlTypes;
    value?: | string;
    options?: any[];
    defaultValue?: any;
}

/**
 * Reducer action
 */
export interface Action {
    type: string,
    payload?: any;
}

export class CreateAction {
    type: string;
    payload: any;
    constructor(type: string, payload?: any) {
        this.type = type;
        this.payload = payload;
    }
}
