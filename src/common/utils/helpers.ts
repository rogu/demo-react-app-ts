export function search(collection: any[], filters: any) {

    if (!collection.length || !Object.keys(filters).length) return collection;

    return collection.filter((el) => {
        for (const key in filters) {
            const filter = filters[key]?.toString().toLowerCase();
            const value = el[key]?.toString().toLowerCase();
            if (!value?.includes(filter)) return false;
        }
        return true;
    })

}
export const delay = (cb: Function, time = 500) => {
    setTimeout(() => cb(), time);
}
