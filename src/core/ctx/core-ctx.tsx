import axios, { AxiosResponse } from "axios";
import {
  createContext,
  Dispatch,
  useCallback,
  useContext,
  useEffect,
  useReducer,
} from "react";
import pkg from "../../../package.json";
import { useHttpInterceptors } from "../config/http-interceptor";
import { Api } from "../../common/utils/api";
import { useLocalStorage } from "../../common/hooks/use-local-storage";
import {
  Action,
  CoreStateAction,
  CoreStateModel,
  CreateAction,
  HttpResponseModel,
} from "../../common/types/models";
import { useTranslation } from "react-i18next";
import { coreReducer } from "./core-reducer";
import { useSnackbar } from "notistack";
import { useNavigate } from "react-router-dom";

export interface CoreCtxModel {
  setToken: (a: string) => void;
  logIn: (data: any) => void;
  logOut: () => void;
  registerUser: (val: any, x: any) => Promise<void>;
  dispatch: Dispatch<Action>;
  state: CoreStateModel;
}

const CoreCtx = createContext({} as CoreCtxModel);

export const CoreCtxProvider = ({ children }: any) => {
  const [token, setToken] = useLocalStorage("token");
  const { i18n } = useTranslation();
  const [state, dispatch] = useReducer(coreReducer, {
    loading: false,
    theme: "light",
    loggedIn: undefined,
    lang: "en",
    version: pkg.version,
  });
  useHttpInterceptors(dispatch);
  const { enqueueSnackbar } = useSnackbar();
  const navi = useNavigate();

  useEffect(() => {
    axios.defaults.headers.common["Authorization"] = token;
  }, [token]);

  useEffect(() => {
    i18n.changeLanguage(state.lang);
  }, [state.lang, i18n]);

  useEffect(() => {
    dispatch(new CreateAction(CoreStateAction.SET_LOGGEDIN, true));
    axios
      .get<any, HttpResponseModel>(Api.LOGGED_END_POINT)
      .then(({ warning }) =>
        dispatch(
          new CreateAction(CoreStateAction.SET_LOGGEDIN, warning ? false : true)
        )
      );
  }, []);

  const logIn = useCallback(
    (data: any) => {
      axios.post<HttpResponseModel>(Api.LOGIN_END_POINT, data).then(
        ({ data: { accessToken } }: AxiosResponse<HttpResponseModel>) => {
          dispatch(new CreateAction(CoreStateAction.SET_LOGGEDIN, true));
          navi("/");
          setToken(accessToken);
        }
      );
    },
    [setToken]
  );

  const logOut = useCallback(() => {
    dispatch(new CreateAction(CoreStateAction.SET_LOGGEDIN, false));
    setToken("");
    enqueueSnackbar("your are logged out", { variant: "success" });
  }, [dispatch, setToken, enqueueSnackbar]);

  const registerUser = useCallback(async (val: any, ev: any) => {
    await axios.post(Api.REGISTER_END_POINT, {
      ...val,
      birthDate: Date.parse(val.birthDate),
    });
    ev.target.reset();
  }, []);

  return (
    <CoreCtx.Provider
      value={{
        setToken,
        logIn,
        logOut,
        registerUser,
        dispatch,
        state,
      }}
    >
      {children}
    </CoreCtx.Provider>
  );
};

export const CoreCtxConsumer = CoreCtx.Consumer;

export const useCoreCtx = () => useContext(CoreCtx);
