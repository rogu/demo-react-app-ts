
import { Action, CoreStateAction, CoreStateModel } from './../../common/types/models';

export const coreReducer = (state: CoreStateModel, action: Action): CoreStateModel => {
    switch (action.type) {
        case CoreStateAction.SET_LOADING:
            return { ...state, loading: action.payload }

        case CoreStateAction.SET_THEME:
            return { ...state, theme: action.payload }

        case CoreStateAction.SET_LOGGEDIN:
            return { ...state, loggedIn: action.payload }

        case CoreStateAction.SET_LANG:
            return { ...state, lang: action.payload }

        default:
            return { ...state }
    }
}
