import { Box, Container, Divider } from "@mui/material";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import { Home } from "../../pages/home";
import Item from "../../pages/items/item";
import { Items } from "../../pages/items/items";
import { ItemsCtxProvider } from "../../pages/items/items-ctx";
import Profile from "../../pages/profile/profile";
import { ProfileProvider } from "../../pages/profile/profile-ctx";
import { Register } from "../../pages/auth/register";
import Workers from "../../pages/workers/workers";
import { WorkersCtxProvider } from "../../pages/workers/workers-ctx";
import { NotFound } from "./not-found";
import Auth from "../../pages/auth/auth";
import ResponsiveAppBar from "./app-bar";
import Spinner from "../../common/ui/spinner";
import Cats from "../../pages/cats/cats";
import { CatsCtxProvider } from "../../pages/cats/cats.ctx";
import { InfinityCtxProvider } from "../../pages/infinity/infinity.ctx";
import InfinityList from "../../pages/infinity/infinity";

function Layout({ state: { loggedIn, version, loading } }: any) {
  return (
    <div>
        {(() => {
          switch (true) {
            case loggedIn:
              return (
                <>
                  <ResponsiveAppBar></ResponsiveAppBar>
                  <Container sx={{ padding: 2, position: "relative", overflow: 'hidden' }}>
                    {loading && <Spinner />}
                    <Outlet />
                    <Divider sx={{ my: 3 }} />
                    <div className="text-gray-400">v. {version}</div>
                    <course-footer courses></course-footer>
                  </Container>
                </>
              );
            case loggedIn === undefined:
              return "";
            default:
              return <Navigate to="/auth" replace />;
          }
        })()}

    </div>
  );
}

export function RouterConfig({ state }: any) {
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout state={state} />}>
          <Route index element={<Home />} />
          <Route
            path="items"
            element={
              <ItemsCtxProvider>
                <Box
                  sx={{
                    display: { sm: "flex", xs: "column" },
                    gap: 2,
                    justifyItems: "flex-start",
                  }}
                >
                  <Box sx={{ flexGrow: 1 }}>
                    <Items loggedIn={state.loggedIn} />
                  </Box>
                  <Box sx={{ flexBasis: "1/4" }}>
                    <Outlet />
                  </Box>
                </Box>
              </ItemsCtxProvider>
            }
          >
            <Route path=":id" element={<Item />} />
          </Route>
          <Route
            path="workers"
            element={
              <WorkersCtxProvider>
                <Workers />
              </WorkersCtxProvider>
            }
          />
          <Route
            path="cats"
            element={
              <CatsCtxProvider>
                <Cats/>
              </CatsCtxProvider>
            }
          />
          <Route
            path="infinity"
            element={
              <InfinityCtxProvider>
                <InfinityList />
              </InfinityCtxProvider>
            }
          />
          <Route path="register" element={<Register />} />
          <Route
            path="profile"
            element={
              <ProfileProvider>
                <Profile />
              </ProfileProvider>
            }
          />

          <Route path="*" element={<NotFound />} />
        </Route>
        <Route path="auth" element={<Auth />}>
          <Route path=":id" element={<Auth />} />
        </Route>
      </Routes>
    </>
  );
}
