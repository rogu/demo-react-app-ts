import { Typography } from "@mui/material";
import { Link } from "react-router-dom";

export const NotFound = () => {
  return (
    <>
      <Link to='/'>Home</Link>
      <Typography variant="h2">404: page not found!</Typography>
    </>
  );
};
