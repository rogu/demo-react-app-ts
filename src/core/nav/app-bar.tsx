import AppRegistrationIcon from "@mui/icons-material/AppRegistration";
import ContactMailOutlinedIcon from "@mui/icons-material/ContactMailOutlined";
import MenuIcon from "@mui/icons-material/Menu";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import PetsIcon from '@mui/icons-material/Pets';
import AllInclusive from '@mui/icons-material/AllInclusive';
import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Switch,
} from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Toolbar from "@mui/material/Toolbar";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import { MouseEvent, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { CoreStateAction } from "../../common/types/models";
import { ReactComponent as Logo } from "../../logod.svg";
import { useCoreCtx } from "../ctx/core-ctx";

const pages = [
  { text: "items", icon: <AppRegistrationIcon /> },
  { text: "workers", icon: <ContactMailOutlinedIcon /> },
  { text: "cats", icon: <PetsIcon /> },
  { text: "infinity", icon: <AllInclusive /> },
];

const ResponsiveAppBar = () => {
  const [anchorleftNav, setAnchorLeftNav] = useState<null | HTMLElement>(null);
  const [anchorRightNav, setAnchorRightNav] = useState<null | HTMLElement>(
    null
  );

  const ctx = useCoreCtx();
  const navi = useNavigate();
  const setTheme = (ev: any, checked: any) => {
    ctx.dispatch({
      type: CoreStateAction.SET_THEME,
      payload: checked ? "light" : "dark",
    });
  };

  const handleOpenLeftNav = ({ currentTarget }: MouseEvent<HTMLElement>) => {
    setAnchorLeftNav(currentTarget);
  };
  const handleOpenRightNav = ({ currentTarget }: MouseEvent<HTMLElement>) => {
    setAnchorRightNav(currentTarget);
  };

  const handleCloseLeftNav = () => {
    setAnchorLeftNav(null);
  };

  const handleCloseRightNav = () => {
    setAnchorRightNav(null);
  };

  function logOut() {
    ctx.logOut();
    handleCloseRightNav();
    navi('/auth/0');
  }

  const mobileLeftNav = (
    <Box
      sx={{
        flexGrow: 1,
        display: { xs: "flex", md: "none" },
      }}
    >
      <IconButton size="large" onClick={handleOpenLeftNav} color="inherit">
        <MenuIcon />
      </IconButton>
      <Drawer anchor="left" open={!!anchorleftNav} onClose={handleCloseLeftNav}>
        <List>
          <ListItem>
            <Logo></Logo>
          </ListItem>
          {pages.map(({ text, icon }) => (
            <ListItem
              component={NavLink}
              onClick={handleCloseLeftNav}
              key={text}
              to={`/${text}`}
            >
              <ListItemIcon>{icon}</ListItemIcon>
              <ListItemText
                primary={text.replace(/\b\w/g, (l) => l.toUpperCase())}
              />
            </ListItem>
          ))}
        </List>
      </Drawer>
    </Box>
  );

  const leftNav = (
    <Box
      sx={{
        flexGrow: 1,
        display: { xs: "none", md: "flex" },
        alignItems: "center",
      }}
    >
      <Link to="/" className="mr-3">
        <Logo fill="#ff0000" className="logo" stroke="red" color="red" />
      </Link>
      {pages.map(({ text }) => (
        <NavLink
          key={text}
          style={{
            padding: "8px 12px",
            color: "white",
            textDecoration: "none",
            textTransform: "uppercase",
          }}
          to={`/${text}`}
        >
          {text}
        </NavLink>
      ))}
    </Box>
  );

  const rightNav = (
    <Box sx={{ flexGrow: 0, display: "flex" }}>
      <Box>
        <span>{ctx.state.theme === "dark" ? "🌙" : "🌞"}</span>
        <Switch
          color="default"
          checked={ctx.state.theme === "light" ? true : false}
          onChange={setTheme}
          inputProps={{ "aria-label": "controlled" }}
        />
      </Box>
      <Divider
        sx={{ mr: 1 }}
        orientation="vertical"
        variant="middle"
        color="white"
        flexItem
      />
      <Box sx={{ lineHeight: 1, alignItems: "center", display: "flex" }}>
        {!ctx.state.loggedIn && "not"} logged in &nbsp;
      </Box>
      <Tooltip title="Open settings">
        <IconButton color="inherit" onClick={handleOpenRightNav} sx={{ p: 0 }}>
          <MoreVertIcon />
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={anchorRightNav}
        open={!!anchorRightNav}
        onClose={handleCloseRightNav}
      >
        <MenuItem onClick={handleCloseRightNav}>
          <Typography textAlign="center">
            <NavLink to="profile">profile</NavLink>
          </Typography>
        </MenuItem>
        <MenuItem>
          {ctx.state.loggedIn ? (
            <Typography textAlign="center" onClick={logOut}>
              log out
            </Typography>
          ) : (
            <Typography textAlign="center" onClick={handleCloseRightNav}>
              <NavLink to="auth/0">login</NavLink>
            </Typography>
          )}
        </MenuItem>
      </Menu>
    </Box>
  );

  return (
    <AppBar position="static">
      <Container maxWidth="lg">
        <Toolbar disableGutters>
          {mobileLeftNav}
          {leftNav}
          {rightNav}
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default ResponsiveAppBar;
