import { delay } from './../../common/utils/helpers';
import axios, { AxiosResponse } from "axios";
import { Action, CoreStateAction, CreateAction } from "../../common/types/models";
import { useSnackbar } from 'notistack';
import { useEffect } from 'react';

export const useHttpInterceptors = (dispatch: (a: Action) => void) => {

    const { enqueueSnackbar } = useSnackbar();

    useEffect(() => {
        axios.interceptors.request.use(
            (config) => {
                dispatch(new CreateAction(CoreStateAction.SET_LOADING, true));
                return { ...config };
            },
            (error) => {
                // delay time to show loading state long time
                delay(() => dispatch(new CreateAction(CoreStateAction.SET_LOADING, false)));
                return Promise.reject(error);
            }
        );

        axios.interceptors.response.use(
            ({ data }: AxiosResponse) => {
                delay(() => dispatch(new CreateAction(CoreStateAction.SET_LOADING, false)));
                switch (true) {
                    case !!data?.success:
                        enqueueSnackbar(data?.success, { variant: 'success' });
                        break;
                    case !!data?.warning:
                        enqueueSnackbar(data?.warning, { variant: 'warning' });
                        break;
                    case !!data?.info:
                        enqueueSnackbar(data?.info, { variant: 'info' });
                        break;
                }
                return data;
            },
            (error) => {
                enqueueSnackbar(error?.response?.data?.error, { variant: 'error' });
                delay(() => dispatch(new CreateAction(CoreStateAction.SET_LOADING, false)));
                return Promise.reject(error);
            }
        );
    }, [dispatch, enqueueSnackbar]);
};
