import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them,
// or even better, manage them separated from your code: https://react.i18next.com/guides/multiple-translation-files)
const resources = {
  en: {
    translation: {
      welcome: "Welcome",
      username: "Your email",
      password: "Password",
      passwordConfirm: "Confirm password",
      title: "React, TypeScript & Next.js training - demo app",
      register: "Register",
    },
  },
  pl: {
    translation: {
      welcome: "Witaj",
      title: "Szkolenie React, TypeScript i Next.js - demo aplikacji",
      register: "Zarejestruj się",
      username: "Twór email",
      password: "Hasło",
      passwordConfirm: "Powtórz hasło",
      hobbies: "Zainteresowania",
      birthDate: "Data urodzenia",
    },
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "en", // language to use, more information here: https://www.i18next.com/overview/configuration-options#languages-namespaces-resources
    // you can use the i18n.changeLanguage function to change the language manually: https://www.i18next.com/overview/api#changelanguage
    // if you're using a language detector, do not define the lng option

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
